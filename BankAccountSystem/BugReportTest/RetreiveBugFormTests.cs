﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BankAccountSystem;
using System.Windows.Forms;
using System.IO;
using System.Data;
using MySql.Data.MySqlClient;



namespace BugReportTest
{
    [TestClass]
    public class RetreiveBugFormTests
    {
       //these are passing tests
        [TestMethod]
        public void testCompareFilesMethodPass()
        {
            string file1 = Directory.GetCurrentDirectory() + "\\" + "Doc.txt";
            string file2 = Directory.GetCurrentDirectory() + "\\" + "Doc.txt";
            RetrieveBugForm rbf = new RetrieveBugForm();
           Assert.IsTrue( rbf.compareFiles(file1,file2));
        }

        [TestMethod]
        public void testCheckConnectionPass()
        {
            MySqlConnection connection = connection = new MySqlConnection("server = 127.0.0.1;port=3306;Database=c7152198;username=root;password=");

            RetrieveBugForm rbf = new RetrieveBugForm();
           Assert.IsTrue( rbf.checkConnection());
           connection.Close();

        }

        //failing tests
        [TestMethod]
        public void testCompareFilesMethodFail()
        {
            string file1 = Directory.GetCurrentDirectory() + "\\" + "Doc.txt";
            string file2 = Directory.GetCurrentDirectory() + "\\" + "Doc2.txt";
            RetrieveBugForm rbf = new RetrieveBugForm();
            Assert.IsTrue(rbf.compareFiles(file1, file2));
        }

        
    }
}
