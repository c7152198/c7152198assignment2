﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BankAccountSystem;
using System.Windows.Forms;
using System.IO;
using System.Data;
using MySql.Data.MySqlClient;



namespace BugReportTest
{
    [TestClass]
    public class ViewAmendFormTests
    {
        //these are passing tests
        [TestMethod]
        public void testCreateFilePass()
        {
            string file1 = Directory.GetCurrentDirectory() + "\\" + "Doc.txt";
            string file3 = Directory.GetCurrentDirectory() + "\\" + "W.txt";
            byte[] bytes = File.ReadAllBytes(file1);
            ViewAmendsForm rbf = new ViewAmendsForm();
            rbf.CreateFile(file3,bytes);
            Assert.IsTrue(File.Exists(file3));
        }

        //failing tests
        [TestMethod]
        public void testCreateFileFail()
        {
            string file1 = Directory.GetCurrentDirectory() + "\\" + "Doc.txt";
            string file3 = Directory.GetCurrentDirectory() + "\\" + "Fail.txt";
            byte[] bytes = File.ReadAllBytes(file3);
            ViewAmendsForm rbf = new ViewAmendsForm();
            rbf.CreateFile(file3, bytes);
            Assert.IsTrue(File.Exists(file3));
        }


    }
}
