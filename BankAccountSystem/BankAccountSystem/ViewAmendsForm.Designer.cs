﻿namespace BankAccountSystem
{
    partial class ViewAmendsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.amendedRTB = new SyntaxHighlighter.SyntaxRichTextBox();
            this.amendCB = new System.Windows.Forms.ComboBox();
            this.bugLB = new System.Windows.Forms.ListBox();
            this.viewAmendBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.getBugsBtn = new System.Windows.Forms.Button();
            this.openFileBtn = new System.Windows.Forms.Button();
            this.viewFileBtn = new System.Windows.Forms.Button();
            this.amendLB = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // amendedRTB
            // 
            this.amendedRTB.BackColor = System.Drawing.Color.Silver;
            this.amendedRTB.Location = new System.Drawing.Point(673, 51);
            this.amendedRTB.Name = "amendedRTB";
            this.amendedRTB.ReadOnly = true;
            this.amendedRTB.Size = new System.Drawing.Size(427, 290);
            this.amendedRTB.TabIndex = 2;
            this.amendedRTB.Text = "";
            // 
            // amendCB
            // 
            this.amendCB.FormattingEnabled = true;
            this.amendCB.Location = new System.Drawing.Point(86, 10);
            this.amendCB.Name = "amendCB";
            this.amendCB.Size = new System.Drawing.Size(121, 21);
            this.amendCB.TabIndex = 3;
            // 
            // bugLB
            // 
            this.bugLB.BackColor = System.Drawing.Color.Silver;
            this.bugLB.FormattingEnabled = true;
            this.bugLB.Location = new System.Drawing.Point(12, 43);
            this.bugLB.Name = "bugLB";
            this.bugLB.Size = new System.Drawing.Size(250, 290);
            this.bugLB.TabIndex = 4;
            // 
            // viewAmendBtn
            // 
            this.viewAmendBtn.Location = new System.Drawing.Point(277, 142);
            this.viewAmendBtn.Name = "viewAmendBtn";
            this.viewAmendBtn.Size = new System.Drawing.Size(103, 23);
            this.viewAmendBtn.TabIndex = 5;
            this.viewAmendBtn.Text = "View Amends >>";
            this.viewAmendBtn.UseVisualStyleBackColor = true;
            this.viewAmendBtn.Click += new System.EventHandler(this.viewAmendBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(392, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Amend Dates";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(670, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Amended File";
            // 
            // getBugsBtn
            // 
            this.getBugsBtn.Location = new System.Drawing.Point(213, 10);
            this.getBugsBtn.Name = "getBugsBtn";
            this.getBugsBtn.Size = new System.Drawing.Size(49, 23);
            this.getBugsBtn.TabIndex = 8;
            this.getBugsBtn.Text = "GO";
            this.getBugsBtn.UseVisualStyleBackColor = true;
            this.getBugsBtn.Click += new System.EventHandler(this.getBugsBtn_Click);
            // 
            // openFileBtn
            // 
            this.openFileBtn.Location = new System.Drawing.Point(592, 186);
            this.openFileBtn.Name = "openFileBtn";
            this.openFileBtn.Size = new System.Drawing.Size(75, 23);
            this.openFileBtn.TabIndex = 9;
            this.openFileBtn.Text = "Open File";
            this.openFileBtn.UseVisualStyleBackColor = true;
            this.openFileBtn.Click += new System.EventHandler(this.openFileBtn_Click);
            // 
            // viewFileBtn
            // 
            this.viewFileBtn.Location = new System.Drawing.Point(592, 89);
            this.viewFileBtn.Name = "viewFileBtn";
            this.viewFileBtn.Size = new System.Drawing.Size(75, 23);
            this.viewFileBtn.TabIndex = 10;
            this.viewFileBtn.Text = "View File>>";
            this.viewFileBtn.UseVisualStyleBackColor = true;
            this.viewFileBtn.Click += new System.EventHandler(this.viewFileBtn_Click);
            // 
            // amendLB
            // 
            this.amendLB.BackColor = System.Drawing.Color.Silver;
            this.amendLB.FormattingEnabled = true;
            this.amendLB.Location = new System.Drawing.Point(395, 51);
            this.amendLB.Name = "amendLB";
            this.amendLB.Size = new System.Drawing.Size(191, 290);
            this.amendLB.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Project Name";
            // 
            // ViewAmendsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1117, 347);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.amendLB);
            this.Controls.Add(this.viewFileBtn);
            this.Controls.Add(this.openFileBtn);
            this.Controls.Add(this.getBugsBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.viewAmendBtn);
            this.Controls.Add(this.bugLB);
            this.Controls.Add(this.amendCB);
            this.Controls.Add(this.amendedRTB);
            this.Name = "ViewAmendsForm";
            this.Text = "ViewAmendsForm";
            this.Load += new System.EventHandler(this.ViewAmendsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox amendCB;
        private System.Windows.Forms.ListBox bugLB;
        private System.Windows.Forms.Button viewAmendBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private SyntaxHighlighter.SyntaxRichTextBox amendedRTB;
        private System.Windows.Forms.Button getBugsBtn;
        private System.Windows.Forms.Button openFileBtn;
        private System.Windows.Forms.Button viewFileBtn;
        private System.Windows.Forms.ListBox amendLB;
        private System.Windows.Forms.Label label3;
    }
}