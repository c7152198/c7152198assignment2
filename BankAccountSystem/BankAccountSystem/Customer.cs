﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankAccountSystem
{
    class Customer
    {

        //------------------------- Customer getters/setters

       
        //customer name
        private string firstName;
        public void SetFirstName(string ac)
        {
            this.firstName = ac;
        }
        public string GetFirstName()
        {
            return this.firstName;
        }


        private string lastName;
        public void SetLastName(string ac)
        {
            this.firstName = ac;
        }
        public string GetLastName()
        {
            return this.lastName;
        }


        //customer address
        private string houseNumber;
        public void SetHouseNumber(string ac)
        {
            this.houseNumber = ac;
        }
        public string GetHouseNumber()
        {
            return this.houseNumber;
        }



        private string streetName;
        public void SetStreetName(string ac)
        {
            this.streetName = ac;
        }
        public string GetStreetName()
        {
            return this.streetName;
        }


        private string city;
        public void SetCity(string ac)
        {
            this.city = ac;
        }
        public string GetCity()
        {
            return this.city;
        }



        private string state;
        public void SetState(string ac)
        {
            this.state = ac;
        }
        public string GetState()
        {
            return this.state;
        }


        private string country;
        public void SetCountry(string ac)
        {
            this.country = ac;
        }
        public string GetCountry()
        {
            return this.country;
        }

        public Customer()
        {

        }

        //customer credentials
        private int birthYear;
        public void SetYear(int ac)
        {
            this.birthYear = ac;
        }
        public int GetYear()
        {
            return this.birthYear;
        }



        private string birthMonth;
        public void SetMonth(string ac)
        {
            this.birthMonth = ac;
        }
        public string GetMonth()
        {
            return this.birthMonth;
        }


        private string nationality;
        public void SetNationality(string ac)
        {
            this.nationality = ac;
        }
        public string GetNationality()
        {
            return this.nationality;
        }


        private string userName;
        public void SetUserName(string ac)
        {
            this.userName = ac;
        }
        public string GetUserName()
        {
            return this.userName;
        }



        private string password;
        public void SetPassword(string ac)
        {
            this.password = ac;
        }
        public string getPasssword()
        {
            return this.password;
        }



        private string email;
        public void SetEmail(string ac)
        {
            this.email = ac;
        }
        public string GetEmail()
        {//ohi
            return this.email;
        }


        private int phoneNumber;
        public void SetPhoneNumber(int ac)
        {
            this.phoneNumber = ac;
        }
        public int GetPhoneNumber()
        {
            return this.phoneNumber;
        }




    }
}
