﻿namespace BankAccountSystem
{
    partial class BugReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pNameTB = new System.Windows.Forms.TextBox();
            this.authorTB = new System.Windows.Forms.TextBox();
            this.sourceTB = new System.Windows.Forms.TextBox();
            this.bClassTB = new System.Windows.Forms.TextBox();
            this.bLineTB = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.bDescTB = new System.Windows.Forms.TextBox();
            this.reportBugBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.methodTB = new System.Windows.Forms.TextBox();
            this.browseBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(160, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Project Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(160, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Code Author";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(127, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "File Name(with filetype)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(160, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Bug Class";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(160, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Bug Line";
            // 
            // pNameTB
            // 
            this.pNameTB.Location = new System.Drawing.Point(248, 35);
            this.pNameTB.Name = "pNameTB";
            this.pNameTB.Size = new System.Drawing.Size(153, 20);
            this.pNameTB.TabIndex = 5;
            // 
            // authorTB
            // 
            this.authorTB.Location = new System.Drawing.Point(248, 59);
            this.authorTB.Name = "authorTB";
            this.authorTB.Size = new System.Drawing.Size(153, 20);
            this.authorTB.TabIndex = 6;
            // 
            // sourceTB
            // 
            this.sourceTB.Location = new System.Drawing.Point(248, 85);
            this.sourceTB.Name = "sourceTB";
            this.sourceTB.Size = new System.Drawing.Size(153, 20);
            this.sourceTB.TabIndex = 7;
       
            // 
            // bClassTB
            // 
            this.bClassTB.Location = new System.Drawing.Point(248, 111);
            this.bClassTB.Name = "bClassTB";
            this.bClassTB.Size = new System.Drawing.Size(153, 20);
            this.bClassTB.TabIndex = 8;
            // 
            // bLineTB
            // 
            this.bLineTB.Location = new System.Drawing.Point(248, 137);
            this.bLineTB.Name = "bLineTB";
            this.bLineTB.Size = new System.Drawing.Size(153, 20);
            this.bLineTB.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(622, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Carefully fill in the bug details as these details wll be used to retreive the bu" +
    "g and the project it is contained in at a later point in time.";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(160, 223);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Bug Description";
            // 
            // bDescTB
            // 
            this.bDescTB.Location = new System.Drawing.Point(248, 220);
            this.bDescTB.Multiline = true;
            this.bDescTB.Name = "bDescTB";
            this.bDescTB.Size = new System.Drawing.Size(153, 72);
            this.bDescTB.TabIndex = 12;
            // 
            // reportBugBtn
            // 
            this.reportBugBtn.Location = new System.Drawing.Point(420, 299);
            this.reportBugBtn.Name = "reportBugBtn";
            this.reportBugBtn.Size = new System.Drawing.Size(75, 23);
            this.reportBugBtn.TabIndex = 13;
            this.reportBugBtn.Text = "Report Bug";
            this.reportBugBtn.UseVisualStyleBackColor = true;
            this.reportBugBtn.Click += new System.EventHandler(this.reportBugBtn_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(501, 299);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(417, 88);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "(eg. hello.cs)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(417, 118);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "(eg. hello)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(163, 171);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Method Name";
            // 
            // methodTB
            // 
            this.methodTB.Location = new System.Drawing.Point(248, 171);
            this.methodTB.Name = "methodTB";
            this.methodTB.Size = new System.Drawing.Size(153, 20);
            this.methodTB.TabIndex = 18;
            // 
            // browseBtn
            // 
            this.browseBtn.Location = new System.Drawing.Point(491, 88);
            this.browseBtn.Name = "browseBtn";
            this.browseBtn.Size = new System.Drawing.Size(106, 23);
            this.browseBtn.TabIndex = 19;
            this.browseBtn.Text = "Browse SourceFile";
            this.browseBtn.UseVisualStyleBackColor = true;
            this.browseBtn.Click += new System.EventHandler(this.browseBtn_Click);
            // 
            // BugReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 366);
            this.Controls.Add(this.browseBtn);
            this.Controls.Add(this.methodTB);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.reportBugBtn);
            this.Controls.Add(this.bDescTB);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.bLineTB);
            this.Controls.Add(this.bClassTB);
            this.Controls.Add(this.sourceTB);
            this.Controls.Add(this.authorTB);
            this.Controls.Add(this.pNameTB);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "BugReportForm";
            this.Text = "BugReportForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox pNameTB;
        private System.Windows.Forms.TextBox authorTB;
        private System.Windows.Forms.TextBox sourceTB;
        private System.Windows.Forms.TextBox bClassTB;
        private System.Windows.Forms.TextBox bLineTB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox bDescTB;
        private System.Windows.Forms.Button reportBugBtn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox methodTB;
        private System.Windows.Forms.Button browseBtn;
    }
}