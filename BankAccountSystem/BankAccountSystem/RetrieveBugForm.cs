﻿using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System;
using System.Data;
using System.IO;
using System.Drawing;
using EnvDTE;
using EnvDTE80;


namespace BankAccountSystem
{
    public partial class RetrieveBugForm : Form
    {
        byte[] bytes = null;
        string fileName = "";
        string varFilePath = "";
        string varFilePathEditable = "";
        MySqlConnection connection;
        MySqlDataReader reader = null;


        public RetrieveBugForm()
        {
            InitializeComponent();
            connection = new MySqlConnection("server = 127.0.0.1;port=3306;Database=c7152198;username=root;password=");
            selectPjCB.DropDownStyle = ComboBoxStyle.DropDownList;
            populateDropDown();


        }

        public bool checkConnection()
        {
            connection.Open();
            if (connection.State == ConnectionState.Open)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        /// <summary>
        /// connects to the database, retrieves the names of projects and populates the combo box with projet names
        /// </summary>
        private void populateDropDown()
        {
            try
            {
                if (checkConnection())
                {


                    using (MySqlCommand cmd = new MySqlCommand("SELECT projectName FROM project", connection))
                    {
                        reader = cmd.ExecuteReader();
                        //cmd.ExecuteNonQuery();
                        while (reader.Read())
                        {
                            selectPjCB.Items.Add((string)reader["projectName"]);
                        }
                    }
                    //  MessageBox.Show("Bug Successfully Reported");
                    // MessageBox.Show(ColumnName.ToString());


                    // Close();
                }
                else
                {
                    MessageBox.Show("Database connection failed.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            connection.Close();
        }
        /// <summary>
        /// connects to the database, gets a list of bugs and populates the list box with the bugs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void getBugsBtn_Click(object sender, EventArgs e)
        {
            // string description = bugLB.GetItemText(bugLB.SelectedItem);
            string project = selectPjCB.GetItemText(selectPjCB.SelectedItem);


            try
            {
              

                if (checkConnection())
                {
                    bugLB.Items.Clear();


                    using (MySqlCommand cmd = new MySqlCommand("SELECT bug.bugid, bug.bugDesc, project.projectName FROM bug, project WHERE bug.projectName = @project AND bug.projectName = project.projectName", connection))
                    {
                        cmd.Parameters.AddWithValue("@project", project);
                        reader = cmd.ExecuteReader();
                        //cmd.ExecuteNonQuery();
                        while (reader.Read())
                        {
                            //ColumnName = (string)reader["projectName"];
                            bugLB.Items.Add(
                                 reader["bugDesc"]);

                        }
                    }
                    MessageBox.Show("Bug Retrieved Successfully");
                    //MessageBox.Show(ColumnName.ToString());
                    // listBox1.Items.Add(ColumnName);

                    // Close();
                }
                else
                {
                    MessageBox.Show("Database connection failed.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            connection.Close();
        }
        /// <summary>
        /// displays the details of a bug in a rich text box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void viewBtn_Click(object sender, EventArgs e)
        {

            string description = bugLB.GetItemText(bugLB.SelectedItem);
            string details = "";
            MySqlConnection connection = new MySqlConnection("server = 127.0.0.1;port=3306;Database=c7152198;username=root;password=");


            try
            {
                MySqlDataReader reader = null;
                //string ColumnName = "";
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    //bugLB.Items.Clear();


                    using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM bug WHERE bugDesc = @description", connection))
                    {
                        cmd.Parameters.AddWithValue("@description", description);
                        reader = cmd.ExecuteReader();
                        //cmd.ExecuteNonQuery();
                        while (reader.Read())
                        {
                            //ColumnName = (string)reader["projectName"];
                            details += "BUG DETAILS" + Environment.NewLine;
                            details += "Bug ID: " + (string)reader["bugid"].ToString() + Environment.NewLine;
                            details += "Description: " + (string)reader["bugDesc"] + Environment.NewLine;
                            details += "Line: " + reader["bugLine"].ToString() + Environment.NewLine;
                            details += "Class: " + (string)reader["bugClass"] + Environment.NewLine;
                            details += "SourceFile: " + (string)reader["bugClass"] + ".cs" + Environment.NewLine;
                            details += "MethodName: " + (string)reader["method"] + Environment.NewLine;
                            details += "Project: " + (string)reader["projectName"] + Environment.NewLine;
                            displayRTB.Text = details;


                        }
                    }
                    MessageBox.Show("Bug Retrieved Successfully");

                }
                else
                {
                    MessageBox.Show("Database connection failed.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            connection.Close();
        }
        /// <summary>
        /// sets the attributes of the rich text box on form load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RetrieveBugForm_Load(object sender, EventArgs e)
        {

            // Add the keywords to the list.
            displayRTB.Settings.Keywords.Add("using");
            displayRTB.Settings.Keywords.Add("if");
            displayRTB.Settings.Keywords.Add("then");
            displayRTB.Settings.Keywords.Add("else");
            displayRTB.Settings.Keywords.Add("elseif");
            displayRTB.Settings.Keywords.Add("end");
            displayRTB.Settings.Keywords.Add("private");
            displayRTB.Settings.Keywords.Add("public");
            displayRTB.Settings.Keywords.Add("string");
            displayRTB.Settings.Keywords.Add("void");
            displayRTB.Settings.Keywords.Add("return");
            displayRTB.Settings.Keywords.Add("true");
            displayRTB.Settings.Keywords.Add("false");
            displayRTB.Settings.Keywords.Add("int");
            displayRTB.Settings.Keywords.Add("double");
            displayRTB.Settings.Keywords.Add("object");
            displayRTB.Settings.Keywords.Add("protected");
            displayRTB.Settings.Keywords.Add("try");
            displayRTB.Settings.Keywords.Add("catch");
            displayRTB.Settings.Keywords.Add("null");
            displayRTB.Settings.Keywords.Add("namespace");
            displayRTB.Settings.Keywords.Add("byte");
            displayRTB.Settings.Keywords.Add("elseif");
            displayRTB.Settings.Keywords.Add("end");
            displayRTB.Settings.Keywords.Add("this");
            displayRTB.Settings.Keywords.Add("class");
            displayRTB.Settings.Keywords.Add("static");
            displayRTB.Settings.Keywords.Add("float");
            displayRTB.Settings.Keywords.Add("get");
            displayRTB.Settings.Keywords.Add("set");
            // Set the comment identifier. For Lua this is two minus-signs after each other (--). 
            // For C++ we would set this property to "//".
            displayRTB.Settings.Comment = "//";

            // Set the colors that will be used.
            displayRTB.Settings.KeywordColor = Color.Blue;
            displayRTB.Settings.CommentColor = Color.Green;
            displayRTB.Settings.StringColor = Color.Red;
            displayRTB.Settings.IntegerColor = Color.Blue;

            // Let's not process strings and integers.
            displayRTB.Settings.EnableStrings = true;
            displayRTB.Settings.EnableIntegers = true;

            // Let's make the settings we just set valid by compiling
            // the keywords to a regular expression.
            displayRTB.CompileKeywords();

            // Load a file and update the syntax highlighting.
            //m_syntaxRichTextBox.LoadFile("../script.lua", RichTextBoxStreamType.PlainText);
            displayRTB.ProcessAllLines();
        }
        /// <summary>
        /// displays the contents of the bug class in a rich text box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void previewBugBtn_Click(object sender, EventArgs e)
        {
         
            try
            {
     
                string description = bugLB.GetItemText(bugLB.SelectedItem);

                if (checkConnection())
                {
                    //bugLB.Items.Clear();


                    using (MySqlCommand cmd = new MySqlCommand("SELECT sourceFile,bugClass FROM bug WHERE bugDesc = @description", connection))
                    {
                        cmd.Parameters.AddWithValue("@description", description);
                        reader = cmd.ExecuteReader();
                        //cmd.ExecuteNonQuery();
                        while (reader.Read())
                        {

                            bytes = (byte[])reader["sourceFile"];
                            fileName = (string)reader["bugClass"] + "FromDatabase.cs";
                            varFilePath = fileName;
                        }
 
                        MessageBox.Show(varFilePath);

                    }
                    MessageBox.Show("Bug Retrieved Successfully");

                }
                else
                {
                    MessageBox.Show("Database connection failed.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            connection.Close();
            try
            {
                File.WriteAllBytes(varFilePath, bytes);
                displayRTB.Text = File.ReadAllText(varFilePath);

                // displayRTB.LoadFile(varFilePathPreview, RichTextBoxStreamType.PlainText);
                displayRTB.ProcessAllLines();
            }
            catch
            {
            }

        }
        /// <summary>
        /// opens the bug file in a c# environment
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void launchBugBtn_Click(object sender, EventArgs e)
        {
            int line = 0;
               try
            {

                string description = bugLB.GetItemText(bugLB.SelectedItem);
                varFilePath=null;
                varFilePathEditable=null;

                if (connection.State == ConnectionState.Open)
                {


                    using (MySqlCommand cmd = new MySqlCommand("SELECT sourceFile,bugClass, bugLine FROM bug WHERE bugDesc = @description", connection))
                    {
                        cmd.Parameters.AddWithValue("@description", description);
                        reader = cmd.ExecuteReader();
                        //cmd.ExecuteNonQuery();
                        while (reader.Read())
                        {

                            line = (int)reader["bugLine"];
                            bytes = (byte[])reader["sourceFile"];
                            fileName = (string)reader["bugClass"];
                            varFilePath = Directory.GetCurrentDirectory() + "\\" + fileName + "FromDatabase.cs";
                            varFilePathEditable = Directory.GetCurrentDirectory() + "\\" + fileName  + "FromDatabaseEditable.cs";
                           

                        }
                        MessageBox.Show(varFilePath);
                        MessageBox.Show(varFilePathEditable);
                    }
                    MessageBox.Show("Bug Retrieved Successfully");

                }
                else
                {
                    MessageBox.Show("Database connection failed.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            connection.Close();
            try {
                File.WriteAllBytes(varFilePath, bytes);
                File.WriteAllBytes(varFilePathEditable, bytes);
            }
            catch
            {

            }
            try
            {
                //fileName = "C:\\Users\\Tola\\Documents\\c7152198assignment2\\BankAccountSystem\\BankAccountSystem\\Customer.cs";
               // string filename = "C:\\Users\\Tola\\Desktop\\HSB.cs";




                Type t = Type.GetTypeFromProgID("VisualStudio.DTE", true);
                DTE2 dte = (DTE2)Activator.CreateInstance(t, true);
                dte= (DTE2)System.Runtime.InteropServices.Marshal.GetActiveObject("VisualStudio.DTE");
                dte.MainWindow.Activate();

                // See http://msdn.microsoft.com/en-us/library/ms228772.aspx for the
                // code for MessageFilter - just paste it into the so_sln namespace.
                MessageFilter.Register();

               // dte.Solution.Open(varFilePath);
                Window w = dte.ItemOperations.OpenFile(varFilePathEditable, "{7651A703-06E5-11D1-8EBD-00A0C90F26EA}");
                ((TextSelection)dte.ActiveDocument.Selection).GotoLine(line, true);



            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }

          
          /**
            Type t = Type.GetTypeFromProgID("VisualStudio.DTE", true);
            DTE2 dte = (DTE2)Activator.CreateInstance(t, true);
            dte.MainWindow.Activate();

            // See http://msdn.microsoft.com/en-us/library/ms228772.aspx for the
            // code for MessageFilter - just paste it into the so_sln namespace.
            MessageFilter.Register();

            dte.Solution.Open(@"C:\\Users\\Tola\\Documents\\omotola-ashiru-fractal\\C7152198FRACTAL\\C7152198FRACTAL.sln");
            Window w = dte.ItemOperations.OpenFile(varFilePath, "{7651A703-06E5-11D1-8EBD-00A0C90F26EA}");
            ((TextSelection)dte.ActiveDocument.Selection).GotoLine(line, true);

    **/
            //dte.Quit();
        }
        /// <summary>
        /// compares two files and decides whether or not they are equal
        /// </summary>
        /// <param name="originalFile"></param>
        /// <param name="editFile"></param>
        /// <returns></returns>
        public bool compareFiles(string originalFile, string editFile)
        {
            try {
                using (var reader1 = new FileStream(originalFile, FileMode.Open, FileAccess.Read))
                {
                    using (var reader2 = new FileStream(editFile, FileMode.Open, FileAccess.Read))
                    {
                        byte[] hash1;
                        byte[] hash2;

                        using (var md51 = new System.Security.Cryptography.MD5CryptoServiceProvider())
                        {
                            md51.ComputeHash(reader1);
                            hash1 = md51.Hash;
                        }

                        using (var md52 = new System.Security.Cryptography.MD5CryptoServiceProvider())
                        {
                            md52.ComputeHash(reader2);
                            hash2 = md52.Hash;
                        }

                        int j = 0;
                        for (j = 0; j < hash1.Length; j++)
                        {
                            if (hash1[j] != hash2[j])
                            {
                                break;
                            }
                        }

                        if (j == hash1.Length)
                        {

                            return true;
                        }
                        else
                        {

                            return false;
                        }
                    }
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.ToString());
                return false;
            }
     
           
        }
         /// <summary>
         /// updates the changes to the bug file in the database
         /// </summary>
         /// <param name="sender"></param>
         /// <param name="e"></param>
        private void updateChangesBtn_Click(object sender, EventArgs e)
        {
           if(compareFiles(varFilePath, varFilePathEditable))
            {
                MessageBox.Show("The are no changes to update");
            }
            else
            {
               
                MySqlConnection connection = new MySqlConnection("server = 127.0.0.1;port=3306;Database=c7152198;username=root;password=");
                try
                {

                    MySqlDataReader reader = null;
                    string description = bugLB.GetItemText(bugLB.SelectedItem);
                    connection.Open();

                    if (connection.State == ConnectionState.Open)
                    {


                        using (MySqlCommand cmd = new MySqlCommand("insert into  audithistory (bugDesc,sourceFile,projectName,date) values(@bugDesc,@sourceFile,@projectName,@date)", connection))
                        {
                            cmd.Parameters.AddWithValue("@bugDesc", description);
                            byte[] bytes = File.ReadAllBytes(varFilePathEditable);
                            cmd.Parameters.AddWithValue("@sourceFile", bytes);
                            cmd.Parameters.AddWithValue("@projectName", selectPjCB.GetItemText(selectPjCB.SelectedItem));
                            DateTime now = DateTime.Now;
                            string nowDate = now.ToString();
                            cmd.Parameters.AddWithValue("@date", nowDate);
                            reader = cmd.ExecuteReader();
                        }
                     
                        MessageBox.Show("Change Inserted successfullly");

                    }
                    else
                    {
                        MessageBox.Show("Database connection failed.");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                connection.Close();
                MessageBox.Show("The are  changes to update");
            }
        }
    }
}
