﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using EnvDTE;
using EnvDTE80;
using MySql.Data.MySqlClient;
using System.IO;


using System.Windows.Forms;

namespace BankAccountSystem
{
    public partial class ViewAmendsForm : Form
    {
        byte[] bytes = null;
        string fileName = "";
        string varFilePath = "";
        MySqlConnection connection;
        MySqlDataReader reader = null;
        public ViewAmendsForm()
        {
            InitializeComponent();
            connection = new MySqlConnection("server = 127.0.0.1;port=3306;Database=c7152198;username=root;password=");
            populateDropDown();
            amendCB.DropDownStyle = ComboBoxStyle.DropDownList;
        }
        public bool checkConnection()
        {
            connection.Open();
            if (connection.State == ConnectionState.Open)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        /// <summary>
        /// connects to the database, retrives project names and populates the combobox with  the project names
        /// </summary>
        private void populateDropDown()
        {


            try
            {
             
                if (checkConnection())
                {


                    using (MySqlCommand cmd = new MySqlCommand("SELECT projectName FROM project", connection))
                    {
                        reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            amendCB.Items.Add((string)reader["projectName"]);
                        }
                    }
                    
                }
                else
                {
                    MessageBox.Show("Database connection failed.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            connection.Close();
        }
        /// <summary>
        /// Sets the properties of the richtext box on load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewAmendsForm_Load(object sender, EventArgs e)
        {

            amendedRTB.Settings.Keywords.Add("using");
            amendedRTB.Settings.Keywords.Add("if");
            amendedRTB.Settings.Keywords.Add("then");
            amendedRTB.Settings.Keywords.Add("else");
            amendedRTB.Settings.Keywords.Add("elseif");
            amendedRTB.Settings.Keywords.Add("end");
            amendedRTB.Settings.Keywords.Add("private");
            amendedRTB.Settings.Keywords.Add("public");
            amendedRTB.Settings.Keywords.Add("string");
            amendedRTB.Settings.Keywords.Add("void");
            amendedRTB.Settings.Keywords.Add("return");
            amendedRTB.Settings.Keywords.Add("true");
            amendedRTB.Settings.Keywords.Add("false");
            amendedRTB.Settings.Keywords.Add("int");
            amendedRTB.Settings.Keywords.Add("double");
            amendedRTB.Settings.Keywords.Add("object");
            amendedRTB.Settings.Keywords.Add("protected");
            amendedRTB.Settings.Keywords.Add("try");
            amendedRTB.Settings.Keywords.Add("catch");
            amendedRTB.Settings.Keywords.Add("null");
            amendedRTB.Settings.Keywords.Add("namespace");
            amendedRTB.Settings.Keywords.Add("byte");
            amendedRTB.Settings.Keywords.Add("elseif");
            amendedRTB.Settings.Keywords.Add("end");
            amendedRTB.Settings.Keywords.Add("this");
            amendedRTB.Settings.Keywords.Add("class");
            amendedRTB.Settings.Keywords.Add("static");
            amendedRTB.Settings.Keywords.Add("float");
            amendedRTB.Settings.Keywords.Add("get");
            amendedRTB.Settings.Keywords.Add("set");
            // Set the comment identifier. For Lua this is two minus-signs after each other (--). 
            // For C++ we would set this property to "//".
            amendedRTB.Settings.Comment = "//";

            // Set the colors that will be used.
            amendedRTB.Settings.KeywordColor = Color.Blue;
            amendedRTB.Settings.CommentColor = Color.Green;
            amendedRTB.Settings.StringColor = Color.Red;
            amendedRTB.Settings.IntegerColor = Color.Blue;

            // Let's not process strings and integers.
            amendedRTB.Settings.EnableStrings = true;
            amendedRTB.Settings.EnableIntegers = true;

            // Let's make the settings we just set valid by compiling
            // the keywords to a regular expression.
            amendedRTB.CompileKeywords();

            // Load a file and update the syntax highlighting.
            //m_syntaxRichTextBox.LoadFile("../script.lua", RichTextBoxStreamType.PlainText);
            amendedRTB.ProcessAllLines();
        }
        /// <summary>
        /// connects the database, gets a list of bugs for specified project and displays the list of bugs in alist box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void getBugsBtn_Click(object sender, EventArgs e)
        {
            // string description = bugLB.GetItemText(bugLB.SelectedItem);
            string project = amendCB.GetItemText(amendCB.SelectedItem);
            try
            {

                if (checkConnection())
                {
                    bugLB.Items.Clear();


                    using (MySqlCommand cmd = new MySqlCommand("SELECT bug.bugid, bug.bugDesc, project.projectName FROM bug, project WHERE bug.projectName = @project AND bug.projectName = project.projectName", connection))
                    {
                        cmd.Parameters.AddWithValue("@project", project);
                        reader = cmd.ExecuteReader();
                        //cmd.ExecuteNonQuery();
                        while (reader.Read())
                        {
                            //ColumnName = (string)reader["projectName"];
                            bugLB.Items.Add(
                                 reader["bugDesc"]);

                        }
                    }
                    MessageBox.Show("Bug Retrieved Successfully");
               
                }
                else
                {
                    MessageBox.Show("Database connection failed.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            connection.Close();
        }
        /// <summary>
        /// connects to the database, retrieves a list of ammends for the specified bugs and displays the amend dates in a list box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void viewAmendBtn_Click(object sender, EventArgs e)
        {
            amendLB.Items.Clear();
            try
            {
                string desc = bugLB.GetItemText(bugLB.SelectedItem);
                if (checkConnection())
                {                   
                    using (MySqlCommand cmd = new MySqlCommand("SELECT date FROM audithistory WHERE bugDesc = @bugDesc ORDER BY date DESC", connection))
                    {
                        cmd.Parameters.AddWithValue("@bugDesc", desc);
                        reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            amendLB.Items.Add(
                                 reader["date"]);

                        }
                    }
                    MessageBox.Show("Bug Retrieved Successfully");

                }
                else
                {
                    MessageBox.Show("Database connection failed.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            connection.Close();
        }
        /// <summary>
        /// connects to the database, file for the specified bug and displays it in a rich text box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void viewFileBtn_Click(object sender, EventArgs e)
        {      

            try
            {
                string date =amendLB.GetItemText(amendLB.SelectedItem);
                if (checkConnection())
                {
                   using (MySqlCommand cmd = new MySqlCommand("SELECT sourceFile,projectName FROM audithistory WHERE date = @date", connection))
                    {
                        cmd.Parameters.AddWithValue("@date", date);
                        reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            bytes = (byte[])reader["sourceFile"];
                            fileName = (string)reader["projectName"];
                            varFilePath = Directory.GetCurrentDirectory() + "\\" + fileName + "FromDatabase.cs";

                        }
                    }
                    MessageBox.Show("Bug Retrieved Successfully");

                }
                else
                {
                    MessageBox.Show("Database connection failed.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            connection.Close();
            try {
               CreateFile(varFilePath, bytes);
                amendedRTB.Text = File.ReadAllText(varFilePath);
                amendedRTB.ProcessAllLines();
            }
            catch
            {

            }
        }
    
        /// <summary>
        /// creates a file in project directory
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="bytes"></param>
        public void CreateFile(string fileName, byte[] bytes)
        {
            File.WriteAllBytes(fileName, bytes);
        }
        
        /// <summary>
        /// connects to the database, gets the specified bug file and opens it in a c# environment
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openFileBtn_Click(object sender, EventArgs e)
        {
            try
            {
                
                string date = amendLB.GetItemText(amendLB.SelectedItem);

                if (checkConnection())
                {
                    using (MySqlCommand cmd = new MySqlCommand("SELECT sourceFile,projectName FROM audithistory WHERE date = @date", connection))
                    {
                        cmd.Parameters.AddWithValue("@date", date);
                        reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            bytes = (byte[])reader["sourceFile"];
                            fileName = (string)reader["projectName"];
                            varFilePath = Directory.GetCurrentDirectory() + "\\" + fileName + "FromDatabase.cs";

                        }
                    }
                    MessageBox.Show("Bug Retrieved Successfully");

                }
                else
                {
                    MessageBox.Show("Database connection failed.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            connection.Close();
           
            try
            {
                CreateFile(varFilePath, bytes);
                Type t = Type.GetTypeFromProgID("VisualStudio.DTE", true);
                DTE2 dte = (DTE2)Activator.CreateInstance(t, true);
                dte = (DTE2)System.Runtime.InteropServices.Marshal.GetActiveObject("VisualStudio.DTE");
                dte.MainWindow.Activate();

                // See http://msdn.microsoft.com/en-us/library/ms228772.aspx for the
                // code for MessageFilter - just paste it into the so_sln namespace.
                MessageFilter.Register();

                // dte.Solution.Open(varFilePath);
                Window w = dte.ItemOperations.OpenFile(varFilePath, "{7651A703-06E5-11D1-8EBD-00A0C90F26EA}");
                //((TextSelection)dte.ActiveDocument.Selection).GotoLine(line, true);



            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }

        }
    }
}
