﻿namespace BankAccountSystem
{
    partial class RetrieveBugForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.selectPjCB = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.getBugsBtn = new System.Windows.Forms.Button();
            this.bugLB = new System.Windows.Forms.ListBox();
            this.previewBugBtn = new System.Windows.Forms.Button();
            this.launchBugBtn = new System.Windows.Forms.Button();
            this.viewBtn = new System.Windows.Forms.Button();
            this.displayRTB = new SyntaxHighlighter.SyntaxRichTextBox();
            this.updateChangesBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // selectPjCB
            // 
            this.selectPjCB.FormattingEnabled = true;
            this.selectPjCB.Location = new System.Drawing.Point(165, 17);
            this.selectPjCB.Name = "selectPjCB";
            this.selectPjCB.Size = new System.Drawing.Size(121, 21);
            this.selectPjCB.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Project";
            // 
            // getBugsBtn
            // 
            this.getBugsBtn.Location = new System.Drawing.Point(292, 17);
            this.getBugsBtn.Name = "getBugsBtn";
            this.getBugsBtn.Size = new System.Drawing.Size(35, 23);
            this.getBugsBtn.TabIndex = 2;
            this.getBugsBtn.Text = "Go";
            this.getBugsBtn.UseVisualStyleBackColor = true;
            this.getBugsBtn.Click += new System.EventHandler(this.getBugsBtn_Click);
            // 
            // bugLB
            // 
            this.bugLB.FormattingEnabled = true;
            this.bugLB.Location = new System.Drawing.Point(15, 45);
            this.bugLB.Name = "bugLB";
            this.bugLB.Size = new System.Drawing.Size(314, 316);
            this.bugLB.TabIndex = 3;
            // 
            // previewBugBtn
            // 
            this.previewBugBtn.Location = new System.Drawing.Point(93, 474);
            this.previewBugBtn.Name = "previewBugBtn";
            this.previewBugBtn.Size = new System.Drawing.Size(86, 23);
            this.previewBugBtn.TabIndex = 4;
            this.previewBugBtn.Text = "Preview Code";
            this.previewBugBtn.UseVisualStyleBackColor = true;
            this.previewBugBtn.Click += new System.EventHandler(this.previewBugBtn_Click);
            // 
            // launchBugBtn
            // 
            this.launchBugBtn.Location = new System.Drawing.Point(180, 445);
            this.launchBugBtn.Name = "launchBugBtn";
            this.launchBugBtn.Size = new System.Drawing.Size(106, 23);
            this.launchBugBtn.TabIndex = 5;
            this.launchBugBtn.Text = "Launch in C#";
            this.launchBugBtn.UseVisualStyleBackColor = true;
            this.launchBugBtn.Click += new System.EventHandler(this.launchBugBtn_Click);
            // 
            // viewBtn
            // 
            this.viewBtn.Location = new System.Drawing.Point(12, 474);
            this.viewBtn.Name = "viewBtn";
            this.viewBtn.Size = new System.Drawing.Size(75, 23);
            this.viewBtn.TabIndex = 7;
            this.viewBtn.Text = "View Details";
            this.viewBtn.UseVisualStyleBackColor = true;
            this.viewBtn.Click += new System.EventHandler(this.viewBtn_Click);
            // 
            // displayRTB
            // 
            this.displayRTB.Location = new System.Drawing.Point(335, 17);
            this.displayRTB.Name = "displayRTB";
            this.displayRTB.ReadOnly = true;
            this.displayRTB.Size = new System.Drawing.Size(900, 480);
            this.displayRTB.TabIndex = 6;
            this.displayRTB.Text = "";
            // 
            // updateChangesBtn
            // 
            this.updateChangesBtn.Location = new System.Drawing.Point(185, 474);
            this.updateChangesBtn.Name = "updateChangesBtn";
            this.updateChangesBtn.Size = new System.Drawing.Size(101, 23);
            this.updateChangesBtn.TabIndex = 8;
            this.updateChangesBtn.Text = "Compare Files";
            this.updateChangesBtn.UseVisualStyleBackColor = true;
            this.updateChangesBtn.Click += new System.EventHandler(this.updateChangesBtn_Click);
            // 
            // RetrieveBugForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1247, 509);
            this.Controls.Add(this.updateChangesBtn);
            this.Controls.Add(this.viewBtn);
            this.Controls.Add(this.displayRTB);
            this.Controls.Add(this.launchBugBtn);
            this.Controls.Add(this.previewBugBtn);
            this.Controls.Add(this.bugLB);
            this.Controls.Add(this.getBugsBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.selectPjCB);
            this.Name = "RetrieveBugForm";
            this.Text = "RetrieveBugForm";
            this.Load += new System.EventHandler(this.RetrieveBugForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox selectPjCB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button getBugsBtn;
        private System.Windows.Forms.ListBox bugLB;
        private System.Windows.Forms.Button previewBugBtn;
        private System.Windows.Forms.Button launchBugBtn;
        private System.Windows.Forms.Button viewBtn;
        private SyntaxHighlighter.SyntaxRichTextBox displayRTB;
        private System.Windows.Forms.Button updateChangesBtn;
    }
}