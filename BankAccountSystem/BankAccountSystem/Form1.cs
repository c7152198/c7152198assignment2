﻿using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;

/// <summary>
/// @author Omotola Ashiru
/// </summary>
namespace BankAccountSystem
{
    public partial class Form1 : Form
    {
        public static object reportForm;
        /// <summary>
        /// Initializes the first form
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            reportForm = this;           
        }

        /// <summary>
        /// on subsequent form closes in makes the previous form visible and enables it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Enabled = true;
            Visible = true;
        }
        /// <summary>
        /// Denending on which radio button is clicked, the corresponding form is opened
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (reportBugRB.Checked) {
                BugReportForm nb = new BugReportForm();
                Enabled = false;
                Visible = false;              
                nb.Show();
                nb.FormClosed += new FormClosedEventHandler(Form1_FormClosed);
            }
            if (retreiveBugRB.Checked)
            {
                RetrieveBugForm rb = new RetrieveBugForm();
                Enabled = false;
                Visible = false;
                rb.Show();
                rb.FormClosed += new FormClosedEventHandler(Form1_FormClosed);          

            }
            if (viewAmendsRB.Checked)
            {
                ViewAmendsForm rb = new ViewAmendsForm();
                Enabled = false;
                Visible = false;
                rb.Show();
                rb.FormClosed += new FormClosedEventHandler(Form1_FormClosed);
            }
          
        }
        /// <summary>
        /// closes the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }


    public class MessageFilter : IOleMessageFilter
    {
        //
        // Class containing the IOleMessageFilter
        // thread error-handling functions.

        // Start the filter.
        public static void Register()
        {
            IOleMessageFilter newFilter = new MessageFilter();
            IOleMessageFilter oldFilter = null;
            CoRegisterMessageFilter(newFilter, out oldFilter);
        }

        // Done with the filter, close it.
        public static void Revoke()
        {
            IOleMessageFilter oldFilter = null;
            CoRegisterMessageFilter(null, out oldFilter);
        }

        //
        // IOleMessageFilter functions.
        // Handle incoming thread requests.
        int IOleMessageFilter.HandleInComingCall(int dwCallType,
          System.IntPtr hTaskCaller, int dwTickCount, System.IntPtr
          lpInterfaceInfo)
        {
            //Return the flag SERVERCALL_ISHANDLED.
            return 0;
        }

        // Thread call was rejected, so try again.
        int IOleMessageFilter.RetryRejectedCall(System.IntPtr
          hTaskCallee, int dwTickCount, int dwRejectType)
        {
            if (dwRejectType == 2)
            // flag = SERVERCALL_RETRYLATER.
            {
                // Retry the thread call immediately if return >=0 & 
                // <100.
                return 99;
            }
            // Too busy; cancel call.
            return -1;
        }

        int IOleMessageFilter.MessagePending(System.IntPtr hTaskCallee,
          int dwTickCount, int dwPendingType)
        {
            //Return the flag PENDINGMSG_WAITDEFPROCESS.
            return 2;
        }

        // Implement the IOleMessageFilter interface.
        [DllImport("Ole32.dll")]
        private static extern int
          CoRegisterMessageFilter(IOleMessageFilter newFilter, out
          IOleMessageFilter oldFilter);
    }

    [ComImport(), Guid("00000016-0000-0000-C000-000000000046"),
    InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]
    interface IOleMessageFilter
    {
        [PreserveSig]
        int HandleInComingCall(
            int dwCallType,
            IntPtr hTaskCaller,
            int dwTickCount,
            IntPtr lpInterfaceInfo);

        [PreserveSig]
        int RetryRejectedCall(
            IntPtr hTaskCallee,
            int dwTickCount,
            int dwRejectType);

        [PreserveSig]
        int MessagePending(
            IntPtr hTaskCallee,
            int dwTickCount,
            int dwPendingType);
    }
}

