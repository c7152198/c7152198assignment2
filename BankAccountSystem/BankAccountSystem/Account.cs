﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankAccountSystem
{
    class Account
    {

        //---------------------------------------Account Getters/Setters----------------------------
        private string accountNumber;
        public void SetAccountNumber(string ac)
        {
           this.accountNumber = ac;
        }
        public string GetAccountNumber()
        {
            return this.accountNumber;
        }


        private string accountType;
        public void SetAccountType(string ac)
        {
            this.accountType = ac;
        }
        public string GetAccountType()
        {
            return this.accountType;
        }





        private string accountHolder;
        public void SetAccountHolder(string ac)
        {
            this.accountHolder = ac;
        }
        public string GetAccountHolder()
        {
            return this.accountHolder;
        }



        private double accountBalance;
        public void SetAccountBalance(double ac)
        {
            this.accountBalance = ac;
        }
        public double GetAccountBalance()
        {
            return this.accountBalance;
        }



        private string accountSortCode;
        public void SetAccountSort(string ac)
        {
            this.accountSortCode = ac;
        }
        public string GetAccountSortCode()
        {
            return this.accountSortCode;
        }

        public Account()
        {


        }

        //--------------------------------------------------Account Methods---------------------------------

        public void Withdraw(double withdrawalAmount)
        {

        }

        public void Deposit(double depositAmount)
        {

        }

        public void Transfer()
        {

        }

        public void GetAccountDetails(string accountNumber)
        {
            string details = "Account Holder: " + GetAccountHolder() + Environment.NewLine +
                              "Account Number: " + GetAccountNumber() + Environment.NewLine +
                              "Account Type: " + GetAccountType() + Environment.NewLine +
                              "Account Balance: " + GetAccountBalance() + Environment.NewLine +
                              "Account SortCode: " + GetAccountSortCode() + Environment.NewLine;
        }
    }
}
