﻿using System;
using System.Data;
using System.Windows.Forms;
using System.IO;
using MySql.Data.MySqlClient;


/// <summary>
/// @author: Omotols Ashiru
/// </summary>
namespace BankAccountSystem
{
    public partial class BugReportForm : Form
    {
        byte[] bytes = null;
        MySqlConnection connection;

        public BugReportForm()
        {
             connection = new MySqlConnection("server = 127.0.0.1;port=3306;Database=c7152198;username=root;password=");

            InitializeComponent();
        }
        public bool checkConnection()
        {
            connection.Open();
            if (connection.State == ConnectionState.Open)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
        /// <summary>
        /// Allows you to choose the file you want to upload
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void browseBtn_Click(object sender, EventArgs e)
        {
            string varFilePath = "";
            using (OpenFileDialog dlg = new OpenFileDialog())
            {


                dlg.Title = "Select File";
                dlg.Filter = "Visual Studio Class Files (*.cs)|*.cs";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    varFilePath = dlg.FileName;
                    sourceTB.Text = dlg.FileName;

                }
                else
                {

                    MessageBox.Show("nothing opened");
                }
            }


            try
            {
                bytes = File.ReadAllBytes(varFilePath);
            }
            catch (IOException)
            {
                MessageBox.Show("fail");
            }




        }

        /// <summary>
        /// A connection is made to the database and the newly input values are inserted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void reportBugBtn_Click(object sender, EventArgs e)
        {
           

                try
                {
                    
                    if (checkConnection())
                    {
                        

                    using (MySqlCommand cmd = new MySqlCommand("insert into  bug (bugDesc,bugLine,bugClass,sourceFile,method,projectName) values(@bugDesc,@bugLine,@bugClass,@sourceFile,@method,@projectName)", connection)) 
                        {
                            cmd.Parameters.AddWithValue("@bugDesc", bDescTB.Text);
                            cmd.Parameters.AddWithValue("@bugLine", bLineTB.Text);
                            cmd.Parameters.AddWithValue("@bugClass", bClassTB.Text);
                            cmd.Parameters.AddWithValue("@sourceFile", bytes);
                            cmd.Parameters.AddWithValue("@method", methodTB.Text);
                            cmd.Parameters.AddWithValue("@projectName", pNameTB.Text);
                            cmd.ExecuteNonQuery();
                        }
                        MessageBox.Show("Bug Successfully Reported");
                    Close();
                    }
                    else
                    {
                        MessageBox.Show("Database connection failed.");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                connection.Close();
            
        
    }

      
    }
}
